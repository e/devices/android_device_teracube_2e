#
# Copyright (C) 2021 Teracube Inc.
# Copyright 2022 - E FOUNDATION
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from 2e device
$(call inherit-product, device/teracube/2e/device.mk)

# Assert
TARGET_OTA_ASSERT_DEVICE := yk673v6_lwg62_64,2e,Teracube_2e

PRODUCT_DEVICE := 2e
PRODUCT_MANUFACTURER := Teracube
PRODUCT_NAME := lineage_2e
PRODUCT_BRAND := Teracube
PRODUCT_MODEL := Teracube_2e

PRODUCT_GMS_CLIENTID_BASE := android-teracube

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRODUCT_DEVICE=Teracube_2e \
    PRODUCT_NAME=Teracube_2e \
    PRIVATE_BUILD_DESC="full_yk673v6_lwg62_64-user 10 QP1A.190711.020 p1k61v164bspP29 release-keys"

BUILD_FINGERPRINT := Teracube/Teracube_2e/Teracube_2e:10/QP1A.190711.020/202011161116:user/release-keys
