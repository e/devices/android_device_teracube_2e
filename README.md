Copyright (C) 2021 The Styx Project  
Copyright 2022 - E FOUNDATION

# Device Tree for Teracube 2e (2e)

The Teracube 2e (codenamed _"2e"_) is a mid-range smartphone from Teracube.
It was released in November 2020.

## Device specifications

| Basic                   | Spec Sheet                                                                                                                     |
| -----------------------:|:------------------------------------------------------------------------------------------------------------------------------ |
| CPU                     | Octa-core 1.80 Ghz Cortex-A53                                                                                                  |
| Chipset                 | Mediatek MT6765G Helio A25                                                                                                     |
| GPU                     | PowerVR GE8320                                                                                                                 |
| Memory                  | 4GB RAM                                                                                                                        |
| Shipped Android Version | 10.0                                                                                                                           |
| Storage                 | 64 GB                                                                                                                          |
| Battery                 | Removable Li-Po 4000 mAh battery                                                                                               |
| Display                 | 720 x 1560 pixels, 6.1"                                                                                                        |
