#
# Copyright (C) 2021 Teracube Inc.
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_2e.mk

COMMON_LUNCH_CHOICES := \
    lineage_2e-eng \
    lineage_2e-user \
    lineage_2e-userdebug
